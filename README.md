# Gandi LiveDNS Dynamic DNS

Exposes an HTTP server that converts DDNS requests to Gandi LiveDNS API calls.

## Configuration

All parameters are set with environment variables.

| Parameter | Required | Description |
| --------- | -------- | ----------- |
| HTTP_ADDR | ✓ | HTTP binding address for the server |
| API_KEY | ✓ | Gandi API key |
| SHARING_ID | | Gandi organization ID |
| SECRET_KEY | | Password to protect server's endpoint |

## Request a DDNS update

In order to update a Gandi LiveDNS record, call the `/update` endpoint.

Setup the following query-string parameters to configure your call:

| Parameter | Description |
| --------- | ----------- |
| hostname | Full hostname to be updated (e.g:. mynas.example.net) |
| ip | The new IPv4 address to register |

## Optional authentication

If you want to avoid the server from being publicly accessible, you can optionally use a basic authentication mechanism.

By providing a secret key as the `SECRET_KEY` environment variable, you'll be required to provide basic authentication details as such:
- username: `api-key`
- password: the configured secret key
