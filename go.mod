module gitlab.com/trilliot/gandi-ddns

go 1.19

require (
	github.com/go-gandi/go-gandi v0.5.0 // indirect
	github.com/peterhellberg/link v1.1.0 // indirect
	golang.org/x/net v0.1.0 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
