package main

import (
	"log"
	"net/http"
	"os"
	"strings"

	gandi "github.com/go-gandi/go-gandi"
	"github.com/go-gandi/go-gandi/config"
	"golang.org/x/net/publicsuffix"
)

var (
	httpAddr  = os.Getenv("HTTP_ADDR")
	apiKey    = os.Getenv("API_KEY")
	sharingID = os.Getenv("SHARING_ID")
	secretKey = os.Getenv("SECRET_KEY")
)

func updateHandler() http.Handler {
	dns := gandi.NewLiveDNSClient(config.Config{
		APIKey:    apiKey,
		SharingID: sharingID,
	})

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Simple authentication mechanism via secret key sent as basic auth username.
		if secretKey != "" {
			user, token, _ := r.BasicAuth()
			if user != "api-key" || token != secretKey {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		}

		// extract domain and ip to be updated
		hostname := strings.TrimSpace(r.URL.Query().Get("hostname"))
		ip := strings.TrimSpace(r.URL.Query().Get("ip"))

		if hostname == "" || ip == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// extract tld and subdomain
		domain, err := publicsuffix.EffectiveTLDPlusOne(hostname)
		if err != nil {
			log.Printf("could not extract domain from %q: %v", hostname, err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		recordName := strings.TrimSuffix(hostname, "."+domain)
		if recordName == "" {
			recordName = "@"
		}

		// update record at gandi
		resp, err := dns.UpdateDomainRecordByNameAndType(domain, recordName, "A", 300, []string{ip})
		if err != nil {
			log.Printf("could not call api: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		log.Printf("A entry for %s updated: %v", hostname, resp.Message)
	})
}

func main() {
	log.Printf("listening on %s", httpAddr)

	http.Handle("/update", updateHandler())
	http.ListenAndServe(httpAddr, nil)
}
