FROM golang:1.19.3-alpine AS build

WORKDIR /app

ARG CI_JOB_USER="gitlab-ci-token"
ARG CI_JOB_TOKEN

COPY go.mod go.sum ./
RUN echo "machine gitlab.com login $CI_JOB_USER password $CI_JOB_TOKEN" >> ~/.netrc && \
    apk add --no-cache ca-certificates tzdata git && \
    go mod download && \
    go mod verify

COPY . .
RUN CGO_ENABLED=0 go build .

# ---

FROM scratch

COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build /app/gandi-ddns /app/

EXPOSE 8080
USER nobody:nobody
ENV HTTP_ADDR=":8080"
CMD ["/app/gandi-ddns"]
